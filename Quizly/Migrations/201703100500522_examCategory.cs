namespace Quizly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class examCategory : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ExamCategoryModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExamCategoryName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ExamCategoryModels");
        }
    }
}
