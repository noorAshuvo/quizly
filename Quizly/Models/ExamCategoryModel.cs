﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Quizly.Models
{
    public class ExamCategoryModel
    {
        public int Id { get; set; }
        public string ExamCategoryName { get; set; }
    }
}