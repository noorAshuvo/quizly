﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Quizly.Startup))]
namespace Quizly
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
