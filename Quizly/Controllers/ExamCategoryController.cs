﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Quizly.Models;

namespace Quizly.Controllers
{
    public class ExamCategoryController : Controller
    {
        private ApplicationDbContext _context;

        public ExamCategoryController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: ExamCategory
        public ActionResult Index()
        {
            // Use database value here. 
            var categoryList = _context.ExamCategoryModels.ToList();

            return View(categoryList);
        }

        public ActionResult Create()
        {        
            return View();
        }
    }
}